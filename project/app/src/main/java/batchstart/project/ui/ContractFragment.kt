package batchstart.project.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import batchstart.project.R
import kotlinx.android.synthetic.main.contract_fragment.*

class ContractFragment: Fragment() {

    private lateinit var parent: HostActivityInterface

    private val TAG = "ContractFragment"

    override fun onAttach(context: Context) {
        super.onAttach(context)
        parent = context as HostActivityInterface
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contract_fragment, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent.getHostViewModel().owner.observe(this.viewLifecycleOwner, Observer { owner ->
            cOwner.text = owner
        })
        parent.getHostViewModel().lastMigration.observe(this.viewLifecycleOwner, Observer { lastMigration ->
            last_migration.text = lastMigration.toString()
        })
        parent.getHostViewModel().getOwnerFromContract()
        parent.getHostViewModel().getLastMigration()
    }
}