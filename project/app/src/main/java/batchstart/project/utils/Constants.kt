package batchstart.project.utils

import java.math.BigInteger

const val CONTACT_ADDRESS = "0x538Da91c22902C45884Aa62828D71F2fC844f48e"
const val ROPSTEN_ENDPOINT = "https://ropsten.infura.io/v3/4d3b4d4c0ac5409a87ee4a53cc02e750"
val GAS_LIMIT: BigInteger = BigInteger.valueOf(20_000_000_000L)
val GAS_PRICE: BigInteger = BigInteger.valueOf(4300000)