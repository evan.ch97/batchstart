package batchstart.project.services.blockchain.wallet

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.LiveData
import org.web3j.crypto.Credentials
import org.web3j.crypto.WalletUtils
import java.io.File
import javax.inject.Inject

class WalletCreator @Inject constructor(context: Context) {
    private val DEFAULT_E_WALLET_FOLDER = File(context.filesDir.absolutePath + "/e-wallet/")

    private  val TAG = "WalletCreator"

    fun generateWallet(password: String, callback: (String) -> Unit) {
        if(!hasWallet()) {
            if(!DEFAULT_E_WALLET_FOLDER.mkdir()) {
                Log.d(TAG, "Failed to create folder at ${DEFAULT_E_WALLET_FOLDER.absolutePath}")
                return
            }
        }
        val string = WalletUtils.generateLightNewWalletFile(password, DEFAULT_E_WALLET_FOLDER)
        Log.d(TAG, string)
        callback(string)
    }

    fun loadCredentials(password: String, path: String = "", callback: (Credentials) -> Unit) {
        val f = if(path == "")  {DEFAULT_E_WALLET_FOLDER} else {File(path)}
        DEFAULT_E_WALLET_FOLDER.walk().forEach {
            if(it.absolutePath.endsWith(".json")) {
                callback(WalletUtils.loadCredentials(password,it.absolutePath))
            }
        }
    }

    fun hasWallet(): Boolean {
        Log.d(TAG, "Checking for wallet. Returned ${DEFAULT_E_WALLET_FOLDER.exists()}.")
        return DEFAULT_E_WALLET_FOLDER.exists()
    }
}